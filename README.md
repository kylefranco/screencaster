ScreenCaster
============
Version 0.5

Author: Kyle Franco

kylefranco@protonmail.com


Description
============
A screen capturing program written in Java. 
The goal of this program is to provide a simple and straight forward
screen recording application that can be used for a variety of tasks.

Documentation
=============

User and developer documentation can be found on the gitlab wiki
"https://gitlab.com/kylefranco/screencaster/wikis/About-ScreenCaster"

Licenses, and other legal information
==============
ScreenCaster is licensed under the GPL V2 Licensing Agreement. 
Libraries which ScreenCaster depends on retain their own licensing agreements
entirely separate from that of ScreenCaster.

The libraries which ScreenCaster currently depends on are

Java Media Framework - Owned and licensed by Oracle.

