package Main.GUI;

import Main.ConfigurationLayer.ControlHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Kyle Franco
 * on 9/07/2016
 */

public class MainWindowController implements Initializable
{
    private ControlHandler controller = new ControlHandler();

    @FXML
    private Button AboutButton;

    @FXML
    private Button HelpButton;

    @FXML
    private Button ScreenshotButton;


    @FXML
    public void initialize(URL fxmlFileLocation, ResourceBundle resourceBundle)
    {
        AboutButton.setOnAction(this::handleAboutButton);
        HelpButton.setOnAction(this::handleHelpButton);
        ScreenshotButton.setOnAction(this::handleScreenshotButton);
    }

    public void handleAboutButton(ActionEvent event)
    {
        try
        {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ScreenCaster_AboutWindow.fxml"));
            Parent root = (Parent) loader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.DECORATED);
            stage.setTitle("About");
            stage.setScene(new Scene(root));
            stage.show();

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /*
     pressing the help button brings up the help PDF/wiki file
     current file is set for testing purposes only
     */
    public void handleHelpButton(ActionEvent event)
    {
        if(Desktop.isDesktopSupported())
        {
            try
            {
                File helpFile = new File("docs/UserDocs/metroid.pdf");
                Desktop.getDesktop().open(helpFile);
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
    }

    //not included in current build. Check beta branch
    /*
    public void handleRecordButton(ActionEvent event)
    {
    }
    */


    // button for taking an image of the entire screen
    public void handleScreenshotButton(ActionEvent event)
    {
        //minimize main window
        Stage stage = (Stage)((Button)event.getSource()).getScene().getWindow();
        stage.setIconified(true);
        //call screenshot method
        controller.callFullScreenshot(stage);
        //show window
        stage.setIconified(false);
    }
}