package Main.ConfigurationLayer;

import Main.ScreenCapture;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;

/**
 * @author Kyle Franco
 * @date 08/25/2018
 *
 * @description this class acts as the intermediary between the events which occur at the
 * view layer, and the corresponding functions in the control layer
 *
 * @note this class does NOT pass data to the view layer from the model layer
 */
public class ControlHandler {
   private ScreenCapture captureScreen = new ScreenCapture();

    /**
     * takeFullScreenShot is called, followed by saveScreenshot, using the local choose
     * directory method
     */
    public void callFullScreenshot(Stage stage)
    {
        //image of the entire primary screen is created
        captureScreen.takeFullScreenShot();
        //image of the screen is saved, and the users chosen save directory is passed via param
        captureScreen.saveScreenShot(chooseDirectory(stage));
    }

    public void callCustomScreenshot()
    {

    }

    /**
     *
     * @return directory location
     *
     */
    private File chooseDirectory(Stage stage)
    {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(stage);
         if(selectedDirectory == null)
         {
             //error
             return null;
         }
         else
         {
             return selectedDirectory;
         }
    }
}
