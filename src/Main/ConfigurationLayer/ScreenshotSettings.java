package Main.ConfigurationLayer;

import java.awt.*;
import java.io.File;

/**
 * Created by Kyle Franco
 * on 7/31/2016.
 */
public class ScreenshotSettings {
    //used to define the universal settings that are essential for the screenshot feature
    private Dimension fullScreenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private File storageLocation;

    public int getScreenHeight() {
        return (int) Math.round(fullScreenSize.height);
    }

    public int getScreenWidth() {
        return (int) Math.round(fullScreenSize.width);
    }

    public Rectangle getFullScreenSize()
    {
        return new Rectangle(fullScreenSize);
    }
}

