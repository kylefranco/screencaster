package Main;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import Main.ConfigurationLayer.ScreenshotSettings;

public class ScreenCapture
{
    private ScreenshotSettings screenshotSettings = new ScreenshotSettings();
    private BufferedImage captureScreen;

    /**
     * takes buffered image of the entire screen. Only captures the primary monitor
     */
    public void takeFullScreenShot()
    {
        try {
            //image of screen is captured
            captureScreen = new Robot().createScreenCapture(screenshotSettings.getFullScreenSize());
        }
        catch (AWTException AWTError)
        {
            AWTError.printStackTrace();
        }
    }

    /**
     *
     * @param saveDirectory
     * saves buffered image to the passed directory location
     */
    public void saveScreenShot(File saveDirectory)
    {
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        try
        {
            ImageIO.write(captureScreen, "jpeg", new File(saveDirectory, date.toString() + ".jpeg"));
        }
        catch (IOException IOError)
        {
            IOError.printStackTrace();
        }
    }
}
